#pragma once

#include <eta/trace/definitions.hpp>
#include <ostream>
#include <string>
#include <unordered_map>

#include "options.hpp"
#include "statistics.hpp"

void print_profile(std::ostream & os, Options const & options, TraceStatistics const & statistics);
