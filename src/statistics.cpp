#include "statistics.hpp"

#include <algorithm>
#include <cassert>
#include <eta/core/debug.hpp>

FunctionCallDuration::FunctionCallDuration(eta::Date d)
        : inclusive_duration(d)
        , exclusive_duration(d) {}

auto FunctionCallDuration::remove_callee_duration(eta::Date d) -> void {
    exclusive_duration.v -= d.v;
    assert(exclusive_duration > 0);
}

CallStackNode::CallStackNode(CallStackNode const & o)
        : function_name(o.function_name)
        , call_id(o.call_id)
        , parent(nullptr)
        , childs()
        , function_calls(o.function_calls) {
    childs.reserve(o.childs.size());
    for (auto const & c : o.childs)
        childs.emplace_back(std::make_unique<CallStackNode>(*c));
}

static auto compare(CallStackNode const & lhs, CallStackNode const & rhs) -> int {
    auto const name_comp = lhs.function_name.compare(rhs.function_name);
    if (name_comp != 0)
        return name_comp;

    auto lit = lhs.childs.begin();
    auto const lend = lhs.childs.end();
    auto rit = rhs.childs.begin();
    auto const rend = rhs.childs.end();

    while (lit != lend && rit != rend) {
        auto cmp = compare(**lit, **rit);
        if (cmp != 0)
            return cmp;
        ++lit;
        ++rit;
    }

    if (rit != rend)
        return -1;
    if (lit != lend)
        return 1;
    return 0;
}

static auto operator<(CallStackNode const & lhs, CallStackNode const & rhs) -> bool {
    return compare(lhs, rhs) < 0;
}

namespace {

struct Builder final {
    eta::Trace const & trace;
    TraceStatistics & statistics;
    std::unique_ptr<CallStackNode> callstack = nullptr;
    CallStackNode * last_callstack_call = nullptr;

    Builder(eta::Trace const & t, TraceStatistics & stats) : trace(t), statistics(stats) {}

    ~Builder() {
        Assert(callstack == nullptr, "Callstack should be empty at the end of the container.");
    }

    auto register_enter_function(eta::Event const & e, ThreadStatistics * thread) -> void {
        auto function_name = std::string {};
        auto exit_event_id = eta::EventId::invalid();

        for (const auto & field : e.fields) {
            if (field.id == trace.field_ids.FunctionName)
                function_name = std::get<eta::String>(field.value);
            else if (field.id == trace.field_ids.ExitFunctionEvent)
                exit_event_id = std::get<eta::EventId>(field.value);
        }

        if(function_name == "") return;
	if(! exit_event_id.isValid()) {
	  eta::log_warning("Warning: no matching exit event for function ", function_name);
	  return;
	}

        auto const & exit_event = trace.event(exit_event_id);
        auto const event_duration = exit_event.timestamp.v - e.timestamp.v;

        auto const function_call_id = FunctionCallId(statistics.function_call_durations.size());
        statistics.function_call_durations.emplace_back(event_duration);

        auto [it, was_inserted] =
                statistics.functions.try_emplace(function_name, FunctionStatistics {});
        it->second.function_calls.emplace_back(function_call_id);

        if (thread != nullptr) {
            auto [it, was_inserted] =
                    thread->functions.try_emplace(function_name, FunctionStatistics {});
            it->second.function_calls.emplace_back(function_call_id);
        }

        if (last_callstack_call != nullptr) {
	  statistics.function_call_durations[last_callstack_call->call_id]
	    .exclusive_duration.v -= event_duration;
        }

        append_to_callstack(function_name, function_call_id);
        last_callstack_call->function_calls.push_back(function_call_id);
    }

    auto register_exit_function(ThreadStatistics * thread) -> void {
        auto const [it, inserted] =
                statistics.function_calls_by_child.try_emplace(*last_callstack_call,
                                                               FunctionCallList {});
        it->second.emplace_back(last_callstack_call->call_id);

        if (thread != nullptr) {
            auto const [it, inserted] =
                    thread->function_calls_by_child.try_emplace(*last_callstack_call,
                                                                FunctionCallList {});
            it->second.emplace_back(last_callstack_call->call_id);
        }

        pop_callstack();
    }

    auto append_to_callstack(std::string_view name, FunctionCallId id) -> void {
        auto new_node = std::make_unique<CallStackNode>(name, id, last_callstack_call);
        auto ptr = new_node.get();
        if (last_callstack_call != nullptr)
            last_callstack_call->childs.push_back(std::move(new_node));
        else
            callstack = std::move(new_node);
        last_callstack_call = ptr;
    }

    auto pop_callstack() -> void {
        last_callstack_call = last_callstack_call->parent;
        if (last_callstack_call == nullptr) {
            callstack.reset();
            assert(last_callstack_call == nullptr);
        }
    }
};

static auto is_thread(eta::Trace const & trace, eta::Container const & cont) -> bool {
    auto const child_container_count =
            std::count_if(trace.containers.begin(),
                          trace.containers.end(),
                          [&cont](auto const & c) { return c.parent == cont.id; });
    return child_container_count == 0;
}

}  // namespace

auto compute_statistics(const eta::Trace & trace) -> TraceStatistics {
    auto statistics = TraceStatistics {};
    for (const auto & cont : trace.containers) {
        auto builder = Builder { trace, statistics };

        auto thread = [&]() -> ThreadStatistics * {
            if (!is_thread(trace, cont))
                return nullptr;
            //
            // add thread total duration to global total duration
            statistics.threads.emplace_back();
            auto & thread_statistics = statistics.threads.back();
            thread_statistics.name = cont.name;
            thread_statistics.duration =
	      cont.events.empty() ? 0
	      : trace.event(cont.events.back()).timestamp.v -
	      trace.event(cont.events.front()).timestamp.v;
            statistics.duration.v += thread_statistics.duration.v;
            return &thread_statistics;
        }();

        for (const auto & event_id : cont.events) {
            auto const & event = trace.event(event_id);
            switch (event.type) {
                case eta::EventType::EnterFunction: {
                    builder.register_enter_function(event, thread);
                } break;
                case eta::EventType::ExitFunction: {
                    builder.register_exit_function(thread);
                } break;
                default: break;
            }
        }
    }

    return statistics;
}
