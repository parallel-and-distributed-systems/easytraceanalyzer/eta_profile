#include <eta/trace/loader.hpp>

#include "function_durations.hpp"
#include "function_type.hpp"
#include "options.hpp"
#include "profile.hpp"
#include "statistics.hpp"

int main(int argc, char ** argv) {
    auto const options = parse_options(argc, argv);

    for (auto const & file : options.files) {
        auto trace = eta::loadTrace(eta::Path(file));

        if (std::holds_alternative<eta::ParsingError>(trace)) {
            std::cerr << "Error parsing " << file << std::endl;
            return 1;
        }

        auto const & t = std::get<eta::Trace>(trace);

        auto const statistics = compute_statistics(t);

        if (options.print_details) {
            print_function_durations(std::cout, statistics, options.exclusive);
        } else {
            print_profile(std::cout, options, statistics);
        }
    }
}
