#include "profile.hpp"

#include <algorithm>
#include <iomanip>
#include <sstream>

// --------------------------------------------------------------------------

struct FunctionDuration final {
    eta::Date min;
    eta::Date max;
    eta::Date total;

    auto add(eta::Date d) -> void {
        total.v += d.v;
        if (d < min)
            min = d;
        else if (d > max)
            max = d;
    }
};

static auto compute_duration(TraceStatistics const & trace_stats,
                             FunctionCallList const & function_calls,
                             bool exclusive) -> FunctionDuration {
    auto const end = function_calls.end();
    auto it = function_calls.begin();
    if (it == end)
        return FunctionDuration { eta::Date(0), eta::Date(0), eta::Date(0) };
    if (exclusive) {
        auto res = FunctionDuration {
            trace_stats.function_call_durations[*it].exclusive_duration,
            trace_stats.function_call_durations[*it].exclusive_duration,
            trace_stats.function_call_durations[*it].exclusive_duration
        };
        while (++it != end)
            res.add(trace_stats.function_call_durations[*it].exclusive_duration);
        return res;
    } else {
        auto res = FunctionDuration {
            trace_stats.function_call_durations[*it].inclusive_duration,
            trace_stats.function_call_durations[*it].inclusive_duration,
            trace_stats.function_call_durations[*it].inclusive_duration
        };
        while (++it != end)
            res.add(trace_stats.function_call_durations[*it].inclusive_duration);
        return res;
    }
}

// --------------------------------------------------------------------------

struct FunctionDisplay final {
    std::string name;
    FunctionDuration duration;
    size_t count;
    float sci;
  FunctionDisplay(std::string _name): name(_name), duration{0,0,0}, count(0), sci(0) {}
  FunctionDisplay(std::string _name,
		  FunctionDuration _duration,
		  size_t _count,
		  float _sci ): name(_name), duration(_duration), count(_count), sci(_sci) {}
};

static auto compute_function_sci(FunctionDisplay const & res, eta::Date full_duration) {
  return (res.duration.total.v - res.count * res.duration.min.v) / full_duration.v;
}

static auto compute_function_display(std::string_view name,
                                     TraceStatistics const & trace_stats,
                                     FunctionStatistics const & function_stats,
                                     eta::Date full_duration,
                                     bool exclusive) -> FunctionDisplay {
    auto res = FunctionDisplay {
        std::string(name),
        compute_duration(trace_stats, function_stats.function_calls, exclusive),
        function_stats.function_calls.size(),
        0,
    };
    res.sci = compute_function_sci(res, full_duration);
    return res;
}

static auto count_compare(FunctionDisplay const & a, FunctionDisplay const & b) -> bool {
    return (a.count == b.count) ? (a.duration.total > b.duration.total) : (a.count > b.count);
}

static auto duration_compare(FunctionDisplay const & a, FunctionDisplay const & b) -> bool {
    return (a.duration.total == b.duration.total) ? (a.count > b.count)
                                                  : (a.duration.total > b.duration.total);
}

// --------------------------------------------------------------------------

template <auto size_0, auto... sizes, typename Type0, typename... Types>
static auto printLine(std::ostream & os, Type0 && first_arg, Types &&... args) -> void {
    os << '|';
    auto ss = std::stringstream {};
    ss << first_arg;
    while (ss.str().size() < size_0)
        ss << ' ';
    os << ss.str() << '|';
    ((os << std::setw(sizes) << std::forward<Types>(args) << '|'), ...);
    os << std::endl;
}

template <auto... sizes> static auto printHorizontalLine(std::ostream & os) -> void {
    auto constexpr line = "------------------------------------------------";
    os << '|';
    ((os << std::string_view(line, sizes) << '|'), ...);
    os << std::endl;
}

static auto print_functions(std::ostream & os,
                            std::vector<FunctionDisplay> const & functions,
                            eta::Date full_duration) -> void {
    printLine<30, 11, 11, 11, 11, 11, 11, 11>(os,
                                              "Function",
                                              "Duration",
                                              "% runtime",
                                              "Count",
                                              "Min",
                                              "Max",
                                              "Average",
                                              "SCI");
    printHorizontalLine<30, 11, 11, 11, 11, 11, 11, 11>(os);

    for (const auto & p : functions) {
        if (p.count > 0)
            printLine<30, 11, 11, 11, 11, 11, 11, 11>(
                    os,
                    p.name,
                    p.duration.total.v,
                    (p.duration.total.v / full_duration.v) * 100,
                    p.count,
                    p.duration.min.v,
	            p.duration.max.v,
	            p.duration.total.v / p.count,
                    p.sci);
        else
            printLine<30, 11, 11, 11, 11, 11, 11, 11>(os, p.name, "", "", "", "", "", "", "");
    }

    os << "Total duration: " << full_duration.v << "\n";
}

static auto print_thread(std::ostream & os,
                         TraceStatistics const & trace_stats,
                         std::unordered_map<std::string, FunctionStatistics> functions,
                         eta::Date full_duration,
                         Options const & options) -> void {
    std::vector<FunctionDisplay> sorted_functions;

    for (auto const & [name, function_stats] : functions)
        sorted_functions.emplace_back(compute_function_display(name,
                                                               trace_stats,
                                                               function_stats,
                                                               full_duration,
                                                               options.exclusive));

    if (options.count_sort) {
        std::sort(sorted_functions.begin(), sorted_functions.end(), count_compare);
    } else {
        std::sort(sorted_functions.begin(), sorted_functions.end(), duration_compare);
    }

    print_functions(os, sorted_functions, full_duration);
}

static auto print_stack(std::ostream & os,
                        TraceStatistics const & statistics,
                        std::map<CallStackNode, FunctionCallList> const & functions,
                        eta::Date full_duration) -> void {
    std::vector<FunctionDisplay> sorted_functions;
    for (auto const & [stack, ids] : functions) {
        {
            auto display = FunctionDisplay {
                stack.function_name,
                compute_duration(statistics, ids, false),
                ids.size(),
                0,
            };
            display.sci = compute_function_sci(display, full_duration);
            sorted_functions.emplace_back(std::move(display));
        }

        for (auto i = 0u; i < stack.childs.size(); ++i) {
            if (i < 2u || i > stack.childs.size() - 3u) {
                auto const & child = stack.childs[i];
                auto display = FunctionDisplay ("  + " + child->function_name);
                sorted_functions.emplace_back(std::move(display));
            } else if (i == 2) {
	      auto display = FunctionDisplay( "    ...");
                sorted_functions.emplace_back(std::move(display));
            }
        }
    }
    print_functions(os, sorted_functions, full_duration);
}

void print_profile(std::ostream & os, Options const & options, TraceStatistics const & statistics) {
    if (options.callee) {
        if (options.profile_thread) {
            for (auto const & stats : statistics.threads) {
                os << "Thread " << stats.name << ":\n";
                print_stack(os, statistics, stats.function_calls_by_child, stats.duration);
                os << std::endl;
            }
        } else {
            print_stack(os, statistics, statistics.function_calls_by_child, statistics.duration);
        }
    } else {
        if (options.profile_thread) {
            for (auto const & stats : statistics.threads) {
                os << "Thread " << stats.name << ":\n";
                print_thread(os, statistics, stats.functions, stats.duration, options);
                os << std::endl;
            }
        } else {
            print_thread(os, statistics, statistics.functions, statistics.duration, options);
        }
    }
}
