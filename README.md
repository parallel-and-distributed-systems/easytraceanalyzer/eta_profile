# eta_profile

*A tool to extract the profile of an application*

Usage: `src/eta_profile [options] [arguments...]`

Available options:

| Option | Description |
|--------|-------------|
|  `-h`, `--help`      | Print the help screen |
|  `-t`, `--thread`    | Compute profile by thread |
|  `-d`, `--details`   | Print more detailed stats (min, max and average values) and dump duration spent in each function in folder `function_durations` |
|  `-e`, `--exclusive` | Compute the exclusive time of execution instead of the inclusive |
|  `-c`, `--count`     | Sort results by count instead of duration |
|  `-g`, `--global`    | Compute a global profile (ie. time spent in OpenMP functions, in MPI functions, in OpenMP synchronisation, in MPI communications, ...) |


## Build eta_profile

In order to build eta_profile you need git, a C++ compiler with C++17 support, [meson](https://mesonbuild.com/) and [ninja](https://ninja-build.org/). Other dependencies will be automatically built by the build system.

```sh
meson build --buildtype release
cd build
meson configure -Dprefix=destination/path
ninja install
```

Optionally can setup otf2 support by installing otf2 and enabling it in eta_profile :
```sh
cd build
meson configure -Deta_trace:otf2=enabled
meson configure -Deta_trace:eta_trace:otf2_incdir=path/to/otf2/include/directory
meson configure -Deta_trace:eta_trace:otf2_libdir=path/to/otf2/library/directory
```

## Output example

```
$ src/eta_profile -g ../traces/eztrace_bt.A.4.trace

     Count       Duration      % runtime                Function
----------.--------------.---------------.------------------------
      9672        6966.78         7.3938%             STV_MPI_Recv
      9672        4644.87        4.92957%             STV_MPI_Send
     19344        2287.81        2.42804%          STV_MPI_Overlap
         8        3.80676     0.00404008%          STV_MPI_Barrier
         8        3.67652     0.00390187%        STV_MPI_Allreduce
        92        2.96396     0.00314563%         STV_EZTRACE_SYNC
        24       0.921975    0.000978486%            STV_MPI_BCast
         4       0.226515    0.000240399%           STV_MPI_Reduce
       808              0              0%          STV_MPI_Waitall

STV_EZTRACE_SYNC not handled for -g option -> Other

      Type     Count  Duration     % runtime
----------.---------.---------.-------------
       MPI     39540   13908.1      14.7606%
     Other        92   2.96396   0.00314563%

Time spent in P2P MPI communications: 29916.4: 31.7501% runtime

Total duration: 94224.7
```

## Plot a distribution curve

After using the `-d` option, the `function_durations` folder is created. In the latter, each file matches a single function. If the `-t` option was used, each folder matches a container.

To plot a distribution curve, use the `eta_profile_plot` script: `./eta_profile_plot function_durations/STV_MPI_Send`.

Output:

![](doc/stv_mpi_send_distribution.png)

In order to plot multiple MPI ranks on the same graph, first use the `-dt` options when using `eta_profile`. Then you have to specify the path to each data file. For instance, you could use: `./eta_profile_plot function_durations/*/STV_MPI_Recv`.

Output:

![](doc/stv_mpi_recv_distribution_multiple.png)

## Test traces

Test traces have been added to the project in order to check the implementation of the tool. These traces are nonsense and do not represent the reality of the execution of a program.

The expected output is as follows:

```
$ src/eta_profile traces/test_trace_01.trace  
Opening ../traces/test_trace_01.trace
     Count       Duration      % runtime                 Function
----------.--------------.--------------.------------------------
         4              4            50%            STV_MPI_BCast

Total duration: 8

$ src/eta_profile -d traces/test_trace_02.trace
Opening traces/test_trace_02.trace
     Count       Duration      % runtime                 Function
----------.--------------.--------------.------------------------
        10              9         56.25%            STV_MPI_BCast

           Min           Max           Avg               Function
--------------.-------------.-------------.-----------------------
           0.5           1.5           0.9           STV_MPI_BCast

Total duration: 16

$ src/eta_profile -te traces/test_trace_03.trace
Opening traces/test_trace_03.trace
==================================================================
Container(2): "P#0_T#4160637728"

     Count       Duration      % runtime                 Function
----------.--------------.--------------.------------------------
         1              5       83.3333%            STV_MPI_BCast

Total duration: 6
==================================================================
Container(4): "P#1_T#4160637728"

     Count       Duration      % runtime                 Function
----------.--------------.--------------.------------------------
         1              5       83.3333%            STV_MPI_BCast

Total duration: 6
==================================================================
Container(6): "P#2_T#4160637728"

     Count       Duration      % runtime                 Function
----------.--------------.--------------.------------------------
         1            2.5       41.6667%            STV_MPI_BCast
         2              2       33.3333%             STV_MPI_Recv
         1            0.5       8.33333%             STV_MPI_Send

Total duration: 6
==================================================================
Container(8): "P#3_T#4160637728"

     Count       Duration      % runtime                 Function
----------.--------------.--------------.------------------------
         2            3.5       58.3333%             STV_MPI_Recv
         1            1.5            25%            STV_MPI_BCast

Total duration: 6
```
