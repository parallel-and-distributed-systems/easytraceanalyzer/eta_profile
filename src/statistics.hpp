#pragma once

#include <eta/trace/definitions.hpp>
#include <map>
//#include <nlc/meta/id.hpp>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

// --------------------------------------------------------------------------

using FunctionCallId = uint64_t;

using FunctionCallList = std::vector<FunctionCallId>;

// --------------------------------------------------------------------------

struct FunctionCallDuration final {
    eta::Date inclusive_duration;
    eta::Date exclusive_duration;

    FunctionCallDuration(eta::Date d);
    auto remove_callee_duration(eta::Date d) -> void;
};

// --------------------------------------------------------------------------

struct CallStackNode final {
    std::string function_name;
    FunctionCallId call_id;
    CallStackNode * parent = nullptr;
    std::vector<std::unique_ptr<CallStackNode>> childs;
    FunctionCallList function_calls;

    CallStackNode(std::string_view name, FunctionCallId id, CallStackNode * p)
            : function_name(name)
            , call_id(id)
            , parent(p) {}

    CallStackNode(CallStackNode &&) = default;
    CallStackNode(CallStackNode const &);
};

// --------------------------------------------------------------------------

struct FunctionStatistics final {
    FunctionCallList function_calls;
};

// --------------------------------------------------------------------------

struct ThreadStatistics final {
    std::string name;
    std::unordered_map<std::string, FunctionStatistics> functions;
    eta::Date duration = 0;
    std::map<CallStackNode, FunctionCallList> function_calls_by_child;
};

// --------------------------------------------------------------------------

struct TraceStatistics final {
    std::unordered_map<std::string, FunctionStatistics> functions;
    std::vector<ThreadStatistics> threads;
    eta::Date duration = 0;
    std::vector<FunctionCallDuration> function_call_durations;
    std::map<CallStackNode, FunctionCallList> function_calls_by_child;
};

auto compute_statistics(const eta::Trace & t) -> TraceStatistics;

// --------------------------------------------------------------------------

