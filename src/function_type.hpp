#pragma once

#include <string_view>

enum class FunctionType { MPI, OMP, OMP_SYNC, STDIO, PTHREAD, OTHER, UNKNOWN };

auto get_function_type(std::string_view) -> FunctionType;
auto to_string(FunctionType) -> std::string_view;

