#pragma once

#include <ostream>

#include "statistics.hpp"

auto print_function_durations(std::ostream & os, TraceStatistics const & statistics, bool exclusive)
        -> void {
    for (auto const & p : statistics.functions) {
        auto it = p.second.function_calls.begin();
        auto const end = p.second.function_calls.end();
        if (it == end)
            continue;

        os << p.first << ':';
        if (exclusive) {
	  os << statistics.function_call_durations[*it].exclusive_duration.v;
            while (++it != end)
                os << ','
                   << statistics.function_call_durations[*it].exclusive_duration.v;
        } else {
            os << statistics.function_call_durations[*it].inclusive_duration.v;
            while (++it != end)
                os << ','
                   << statistics.function_call_durations[*it].inclusive_duration.v;
        }
        os << std::endl;
    }
}

