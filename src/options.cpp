#include "options.hpp"

#include <eta/core/ProgramOptions.hxx>

auto parse_options(int argc, char ** argv) -> Options {
    auto po = po::parser {};
    auto opts = Options {};

    po[""].bind(opts.files);

    auto & help_opt = po["help"].abbreviation('h').description("Print this help screen");

    auto & thread_opt = po["thread"].abbreviation('t').description("Get profile by thread");

    auto & details_opt = po["details"].abbreviation('d').description(
            "Print more detailed stats and generate stats files in "
            "./function_durations to be plotted with plot.py");

    auto & exclusive_opt = po["exclusive"].abbreviation('e').description(
            "Compute the exclusive time of execution instead of the inclusive");

    auto & count_opt =
            po["count"].abbreviation('c').description("Sort results by count instead of duration");

    auto & callee_opt =
            po["sons"].abbreviation('s').description("Print stats function of called functions");

    if (!po(argc, argv)) {
        // eta::log_critical("Error occured during cli arguments parsing. Aborting");
        std::exit(1);
    }

    if (help_opt.size() > 0) {
        std::cout << po << std::endl;
        std::exit(0);
    }

    opts.profile_thread = thread_opt.was_set();
    opts.print_details = details_opt.was_set();
    opts.exclusive = exclusive_opt.was_set();
    opts.count_sort = count_opt.was_set();
    opts.callee = callee_opt.was_set();

    return opts;
}
