#pragma once

#include <string>
#include <vector>

struct Options {
    bool profile_thread = false;
    bool print_details = false;
    bool exclusive = false;
    bool count_sort = false;
    bool callee = false;

    std::vector<std::string> files;
};

auto parse_options(int argc, char ** argv) -> Options;

